import numpy as np
import matplotlib.pyplot as pp
import scipy as sp
from scipy import stats

p = 3.99
c = 3.79

Y = np.array([43,31,46,37,36])

mean = np.mean(Y)
std = np.std(Y)
# ppf, aka quantile, inverse of CDF
q = sp.stats.norm(mean, std).ppf( (p - c)/p )

print(q)

# other stuff

x = np.linspace(0,25,num=500)
cdf = stats.norm(mean, std).cdf(x)

pp.plot(x,cdf)
pp.grid()
pp.show()
