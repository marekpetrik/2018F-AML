# Updated

import numpy as np
import sys, os
import argparse

parser = argparse.ArgumentParser(description = 'Policy Iteration')
parser.add_argument('csv_file')
parser.add_argument('-o', default='out')

args = parser.parse_args()

csv_file = args.csv_file
out_file = args.o

states = {}
actions = {}

# First loop to get number of states and actions
with open(csv_file) as infile:
    first_line = True
    for line in infile:
        if first_line: 
            first_line = False
            continue
        line = line.rstrip(os.linesep)
        columns = line.split(',')
        idstatefrom = int(columns[0])
        idaction = int(columns[1])
        states[idstatefrom] = 0
        actions[idaction] = 0
infile.close()

states_qty = len(states)
actions_qty = len(actions)

P = np.zeros([states_qty, actions_qty, states_qty])
R = np.zeros([states_qty, actions_qty, states_qty])

# Second loop to get transition probabilites and rewards
with open(csv_file) as infile:
    first_line = True
    for line in infile:
        if first_line: 
            first_line = False
            continue
        line = line.rstrip(os.linesep)

        columns = line.split(',')
        idstatefrom = int(columns[0])
        idaction = int(columns[1])
        idstateto = int(columns[2])
        probability = float(columns[3])
        reward = float(columns[4])

        P[idstatefrom, idaction, idstateto] = probability
        R[idstatefrom, idaction, idstateto] = reward
infile.close()

# 1. Initializtion. 
# V and policy uses different types of array. Need to init differently
V = np.zeros(states_qty)
policy = [0] * states_qty

# May want to change these:
eps = 0.01
gamma = 0.75

improving = True
while (improving):
    improving = False
    # 2. Policy Evaluation
    # V_π(s) = ∑_s'∈S{ T(s,π(s),s')[R(s,π(s),s') + γV(s')]}
    for s in range(states_qty):
        sigma = 0
        for s_prime in range(states_qty):
            sigma += P[s,policy[s],s_prime] * (R[s,policy[s],s_prime] + gamma*V[s_prime])
        V[s] = sigma
 
    # 3. Policy Improvement
    for s in range(states_qty):
        best_policy = V[s]
        # For each action, a∈A
        # π'(s) = arg max_a∈A ∑{T(s,a,s')[R(s,a,s') + γV(s')]}
        for a in range(actions_qty):
            sigma = 0
            for s_prime in range(states_qty):
                sigma += P[s, a, s_prime] * (R[s, a, s_prime] + gamma * V[s_prime])
            if sigma > best_policy and abs(sigma - best_policy) > eps:
                policy[s] = a
                best_policy = sigma
                improving = True

print('idstate,idaction')

out = open(out_file, 'w')
out.write('idstate,idaction\n')

for i in range(len(policy)):
    print(i , policy[i])
    out.write(str(i) + ',' + str(policy[i]) + '\n')
out.close()
