# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 15:02:38 2018

@author: Nick
"""
import numpy as np
## Two states with a decision at any time

# TODO: use fractions even when they are integral
# dimensions: action, state from, stateto
P = np.array([[[0.8, 0.2, 1.0],
               [0.4, 0.6, 1.0], 
               [0.0, 0.0, 1.0] ],
 
              [[0.0, 0.0, 1.0],
               [0.0, 0.0, 1.0], 
               [0.0, 0.0, 1.0] ]])


# dimensions: action, state
r = np.array([[200,-20, 0], [6300, 6300, 0]])

## Value iteration

# TODO: See https://docs.scipy.org/doc/numpy-1.13.0/user/basics.indexing.html

assert P.shape[0] == r.shape[0]
assert P.shape[1] == P.shape[2]
assert P.shape[1] == r.shape[1]

gamma = 1/1.02
max_iterations = 10
action_names = ["keep", "sell"]


d_o = np.array([1,1,1])  #decision, map from state to action

p_d = np.zeros( (P.shape[1], P.shape[2]) )
r_d = np.zeros(  P.shape[1] )
v   = np.zeros(  P.shape[2] )

#for t in range(iterations):
#    qvalues = np.array([r[i,:] + gamma * P[i,:,:] @ v for i in range(P.shape[0])])
#    v = np.max(qvalues, axis=0)    
for n in range(max_iterations):
    
    print(n)
    
    #Apply decision to Transition matrix
    for s in range(3-1):
        #print(s)
        #print(P[s])
        p_d[s,:] = P[d_o[s],s,:]
        r_d[s]   = r[d_o[s],s]
        
    #Solve value function    
    v = np.linalg.solve( ( np.eye(3,3) - gamma*p_d ), r_d )
    
    #Set new decision according to argmax
    for s in range(3-1):     
        qval = np.array([r[i,:] + gamma * P[i,:,:] @ v for i in range(P.shape[0])])    
        d    = np.argmax(qval, axis=0)
    
    #is identical?
    if np.all( d_o == d ):
#        print(v)
#        print(d)
#        print(d_o)
        break;
    else:
        d_o = d
        

print( "Policy:", list(np.take(action_names, d)) )
print("Value function:", v)