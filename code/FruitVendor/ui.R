library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
    # Application title
    headerPanel("Strawberry Stand"),
    helpText("You have a farm stand selling strawberries. You need to decide
                how much to stock every day, assuming random demand. Unsold
                strawberries are discarded at the end of the day."), 
    tabsetPanel(
        tabPanel("Demands",
            sidebarLayout(
                sidebarPanel(
                    h3("Historical Demands"),
                    numericInput("demandSamples", label = "Demand samples", value = 3),
                    numericInput("seed", value = NULL, label = "Demand Seed", min = 0, max = 100000),
                    actionButton("generatedemand", label = "New Demands"),
                    downloadButton("downloadDemands", label = "Download")
                ),
                mainPanel(
                    tableOutput("observeddemands")
                )
            )
        ),
        tabPanel("Simulation",
             sidebarLayout(
                 sidebarPanel(
                     h3("Simulation"),
                     numericInput("horizon", value = 100, label = "Horizon", min = 5, max = 1000),
                     numericInput("epochs", value = 100, label = "Epochs", min = 1, max = 1000),
                     numericInput("cost", value = 3.79, label = "Purchase Cost ($)", width = '100%', min = 0, step = 0.01),
                     numericInput("price", value = 3.99, label = "Sale Price ($)", width = '100%', min = 0, step = 0.01),
                     sliderInput("order", value = 1, label = "Order Quantity", width = "100%", min = 0, max = 200),
                     actionButton("simulate", label = "Simulate"),
                     actionButton("resetsimulation", label = "Reset"),
                     actionButton("computeOptimal", label = "Solve")
                 ),
                 mainPanel(
                     plotOutput("money")
                 )
             )     
        )
    )
    

))
